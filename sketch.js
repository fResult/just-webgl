// Ensure ThreeJS is in global scope for the 'examples/'
globalThis.THREE = require('three')

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls')

const canvasSketch = require('canvas-sketch')

const settings = {
  animate: true,
  context: 'webgl',
  pixelsPerInch: 300
}

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    canvas: context.canvas
  })

  // WebGL background color
  renderer.setClearColor('#1c1624"', 1)

  // Setup a camera
  const camera = new THREE.PerspectiveCamera(50, 1, 0.01, 100)
  camera.position.set(0, 2, -8)
  camera.lookAt(new THREE.Vector3())

  // Setup camera controller
  /** @type { import('three/examples/jsm/controls/OrbitControls').OrbitControls } */
  const controls = new THREE.OrbitControls(camera, context.canvas)

  // Setup your scene
  const scene = new THREE.Scene()

  // Setup a geometry
  const geometry = new THREE.SphereGeometry(1, 32, 16)

  // Setup textures
  const textureLoader = new THREE.TextureLoader()
  const earthTexture = textureLoader.load('/assets/textures/earth.jpg')
  const moonTexture = textureLoader.load('/assets/textures/space.jpg')

  const spaceTexture = textureLoader.load('/assets/textures/space.jpg')
  spaceTexture.minFilter = THREE.LinearFilter
  scene.backgroundk = spaceTexture

  const moonGroup = new THREE.Group()

  // Setup a material
  const earthMaterial = new THREE.MeshStandardMaterial({
    roughness: 1,
    metalness: 0,
    map: earthTexture
  })
  const moonMaterial = new THREE.MeshStandardMaterial({
    roughness: 1,
    metalness: 0,
    map: moonTexture
  })

  // Setup a mesh with geometry + material
  const earthMesh = new THREE.Mesh(geometry, earthMaterial)
  const moonMesh = new THREE.Mesh(geometry, moonMaterial)

  const light = new THREE.PointLight('#fff', 2)

  moonMesh.position.set(1.5, 1, 0)
  light.position.set(2, 1.5, 0)

  earthMesh.scale.setScalar(1)
  moonMesh.scale.setScalar(0.25)

  moonGroup.add(moonMesh)
  moonGroup.add(light)
  moonGroup.add(new THREE.GridHelper(5, 10))
  moonGroup.add(new THREE.AxesHelper(5))

  scene.add(earthMesh)
  scene.add(moonGroup)

  scene.add(new THREE.PointLightHelper(light, 0.15))
  scene.add(new THREE.GridHelper(5, 50))
  scene.add(new THREE.AxesHelper(10))

  // draw each frame
  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight, false)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },
    render({ time }) {
      earthMesh.rotation.y = time * 0.15
      moonMesh.rotation.y = -(time * 0.075)
      moonGroup.rotation.y = time

      controls.update()
      renderer.render(scene, camera)
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload() {
      controls.dispose()
      renderer.dispose()
    }
  }
}

canvasSketch(sketch, settings)
