// Ensure ThreeJS is in global scope for the 'examples/'
globalThis.THREE = require('three')

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls')

const canvasSketch = require('canvas-sketch')
const { BufferAttribute } = require('three')

const settings = {
  animate: true,
  context: 'webgl',
  pixelsPerInch: 300
}

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    canvas: context.canvas
  })

  // WebGL background color
  renderer.setClearColor('#fff', 1)

  // Setup a camera
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100)
  camera.position.set(0, 2, -8)
  camera.lookAt(new THREE.Vector3())

  // Setup camera controller
  /** @type { import('three/examples/jsm/controls/OrbitControls').OrbitControls } */
  const controls = new THREE.OrbitControls(camera, context.canvas)
  controls.enableZoom = false

  // Setup your scene
  const scene = new THREE.Scene()

  // A grid
  const gridScale = 10
  scene.add(
    new THREE.GridHelper(gridScale, 10, 'hsl(0, 0%, 50%)', 'hsl(0, 0%, 70%)')
  )

  // Setup a geometry
  const geometry = new THREE.BufferGeometry()

  // Define some vertices & Flatten into buffer attribute
  const vertices = [
    new THREE.Vector3(-0.5, 0.5, 0),
    new THREE.Vector3(0.5, -0.5, 0),
    new THREE.Vector3(-0.5, -0.5, 0),
    new THREE.Vector3(0.5, 0.5, 0)
  ]
  const vertsFlat = vertices.map((p) => p.toArray()).flat()
  const vertsArray = new Float32Array(vertsFlat)
  const vertsAttrib = new BufferAttribute(vertsArray, 3)
  // geometry.position = vertsAttrib
  geometry.setAttribute('position', vertsAttrib)

  // Define some faces & Flatten into beffur attribute
  const faces = [
    [0, 1, 2],
    [0, 3, 1]
  ]
  const facesFlat = faces.flat()
  const facesArray = new Uint16Array(facesFlat)
  const facesAttrib = new THREE.BufferAttribute(facesArray, 1)
  geometry.setIndex(facesAttrib)

  const mesh = new THREE.Mesh(
    geometry,
    new THREE.MeshBasicMaterial({
      color: 0xff00ff,
      side: THREE.DoubleSide
    })
  )
  scene.add(mesh)

  // draw each frame
  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight, false)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },
    render({ time }) {
      controls.update()
      renderer.render(scene, camera)
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload() {
      controls.dispose()
      renderer.dispose()
    }
  }
}

canvasSketch(sketch, settings)
